# Python 3.8 and pip3.8 installer for Debian and Arch based distros

> Saikat Karmakar | Sept : 2021

---

## Usage :
```bash
git clone https://gitlab.com/Aviksaikat/python3.8-installer.git
cd 
sudo ./installer.sh
```
