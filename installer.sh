#!/bin/bash

if ! command -v lolcat &> /dev/null
then
    echo "Lolcat not found installing for fun ;)"
    sudo apt install lolcat || pacman -S lolcat
fi

#? if already installed don't bother
if command -v python3.8 &> /dev/null
then
    echo "python3.8 already installed :-)" | lolcat
    exit
fi

#? checking permissions
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" | lolcat 1>&2 
   exit 1
fi

echo "Installing Python 3.8 with pip3.8 ...Please wait" | lolcat

if [ ! -f Python-3.8.0.tar.xz ]; 
then
    $(which wget) https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tar.xz
fi

if ! command -v tar &> /dev/null
then
    sudo apt install tar || pacman -S tar
fi

$(which tar) -xvf $(pwd)/Python-3.8.0.tar.xz

#? doing the magic here 
cd Python-3.8.0
./configure --enable-optimizations 
sudo make altinstall

sudo rm -r Python-3.8.0  Python-3.8.0.tar.xz
echo "Here is the path" | lolcat 
$(which python3.8)

